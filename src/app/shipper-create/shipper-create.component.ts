import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";

@Component({
  selector: 'app-shipper-create',
  templateUrl: './shipper-create.component.html',
  styleUrls: ['./shipper-create.component.css']
})
export class ShipperCreateComponent implements OnInit {

  @Input() shipperDetails = { id: 0, name: '', phone: '' }

  constructor(
    public restApi: RestApiService, 
    public router: Router
  ) { }

  ngOnInit() { }

  addShipper() {
    this.restApi.createShipper(this.shipperDetails).subscribe((data: {}) => {
      this.router.navigate(['/shipper-list'])
    })
  }

}